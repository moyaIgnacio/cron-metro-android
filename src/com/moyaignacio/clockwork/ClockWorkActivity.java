package com.moyaignacio.clockwork;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;

import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.Window.Callback;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

public class ClockWorkActivity extends Activity implements Callback {
	/** Called when the activity is first created. */

	// variables :D
	Chronometer chron;
	TextView miChron;
	Button button2, button1, reset;
	Boolean running = false, primerVuelta = true;
	String Inicio = new String("Start");
	String Parada = new String("Stop");
	long dec;
	Cronometro myCron;

	// Fin variables :D

	public void onClickUp() {
		if (running == false) {
			// START!!!!
			// START!!!!
			reset.setEnabled(running);
			running = true;
			button1.setText(Parada);
			button2.setEnabled(running);
			// cronometro stock
			if (primerVuelta) {
				chron.setBase(SystemClock.elapsedRealtime());
				chron.start();
			} else {
				chron.start();
			}
			// mi cronometro
			if (!myCron.isAlive()) {
				myCron.start();
			} else {
				miChron.setText("IT'S ALIVE!!");
			}

		} else {
			// STOP!!!!
			// STOP!!!!
			reset.setEnabled(running);
			running = false;
			button1.setText(Inicio);
			// cronometro stock
			chron.stop();
			// mi cronometro
			myCron.Stop();
		}
		button2.setEnabled(running);

	}
	
	public void onClickDown(){
		if(!running){
		//Mi cronometro
		myCron.Reset();
		
		//Cronometro de Android
		chron.setBase(SystemClock.elapsedRealtime());
		}
	}

	View.OnClickListener listener1 = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			onClickUp();
		}
	};
	
	View.OnClickListener listener2 = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			onClickDown();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		
		// XML======================================

		miChron = (TextView) findViewById(R.id.miChron);
		chron = (Chronometer) findViewById(R.id.chronometer1);
		button2 = (Button) findViewById(R.id.button2);
		button1 = (Button) findViewById(R.id.button1);
		reset = (Button) findViewById(R.id.reset);
		button2.setEnabled(running);
		reset.setEnabled(running);

		// ==========================================
		myCron = new Cronometro(miChron);
		
		// Fuente: Roboto Thin
		Typeface font = Typeface
				.createFromAsset(getAssets(), "Roboto-Thin.ttf");
		miChron.setTypeface(font);
		chron.setTypeface(font);
		//
		
		button1.setOnClickListener(listener1);

		reset.setOnClickListener(listener2);

	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		int action = event.getAction();
		int keyCode = event.getKeyCode();
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			if (action == KeyEvent.ACTION_UP) {
				onClickUp();
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			if (action == KeyEvent.ACTION_DOWN) {
				onClickDown();
			}
			return true;
		default:
			return super.dispatchKeyEvent(event);
		}
	}

}

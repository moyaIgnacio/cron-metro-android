package com.moyaignacio.clockwork;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.os.Handler;
import android.os.SystemClock;
import android.widget.TextView;

public class Cronometro extends Thread {

	private Thread stop;
	private Boolean firstRun = true;
	private long timeMilis;
	private Handler myHandler = new Handler();
	private TextView miChron;
	private long tiempo;
	private double segs, min, hs;
	private String post;
	private NumberFormat minf = DecimalFormat.getInstance();
	private NumberFormat hsf = DecimalFormat.getInstance();
	private NumberFormat segsf = DecimalFormat.getInstance();

	private Runnable handlerRunnable = new Runnable() {

		@Override
		public void run() {
			miChron.setText(post);
		}

	};

	public Cronometro(TextView view) {
		miChron = view;
	}

	public void run() {
		Thread thisThread = Thread.currentThread();
		while (stop == thisThread) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (firstRun) {
					timeMilis = SystemClock.elapsedRealtime();
					firstRun = false;
				} else {
					tiempo = SystemClock.elapsedRealtime() - timeMilis;
					hs = tiempo/360000d;
					hs -= hs%1d;
					min = (tiempo - (hs*360000d))/60000d;
					min -= min%1d;
					segs = (tiempo - (hs*360000d) - (min*60000d))/1000d;
					//formatear minutos
					minf.setMaximumIntegerDigits(2);
					minf.setMinimumIntegerDigits(2);
					minf.setMaximumFractionDigits(0);
					minf.setMinimumFractionDigits(0);
					minf.setRoundingMode(RoundingMode.DOWN);
					//formatear horas
					hsf.setMaximumIntegerDigits(1);
					hsf.setMinimumIntegerDigits(1);
					hsf.setMaximumFractionDigits(0);
					hsf.setMinimumFractionDigits(0);
					hsf.setRoundingMode(RoundingMode.DOWN);
					
					//formatear segundos
					segsf.setMaximumIntegerDigits(2);
					segsf.setMinimumIntegerDigits(2);
					segsf.setMaximumFractionDigits(1);
					segsf.setMinimumFractionDigits(1);
					
					post = String.format(hsf.format(hs)+":"+minf.format(min)+":"+segsf.format(segs));
					myHandler.post(handlerRunnable);
				}
			}
		}
	}

	public void Stop() {
		stop = null;
	}

	public void Reset() {
		timeMilis = 0;
		miChron.setText("00:00.00");
		firstRun = true;

	}

	public void start() {
		stop = new Thread(this);
		stop.start();
	}

}
